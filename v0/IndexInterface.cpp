#include"indexinterface.h"



Index::Index(){}

Index::Index(std::string date, int open, int close, int high, int low, int volume, int turnover,
double pe, double pb, double div, int type):m_date(date),m_open(open),m_close(close),m_highValue(high),m_lowValue(low),m_volume(volume),m_turnover(turnover),
m_PE(pe),m_PB(pb),m_dividend(div),m_type(type)
{}
std::string Index::getDate()const{
         return m_date;
     }

int Index::getOpenValue()const{
    return m_open;
}

int Index::getCloseValue()const{
    return m_close;
}

int Index::getHighValue()const{
    return m_highValue;
}

int Index::getLowValue()const{
    return m_lowValue;
}

int Index::getTurnover()const{
    return m_turnover;
}

int Index::getVolume()const{
    return m_volume;
}

double Index::getPriceEarningRatio()const{
    return m_PE;
}

double Index::getPriceBookRatio()const{
    return m_PB;
}

double Index::getDividend()const{
    return m_dividend;
}

int Index::getType()const{
    return m_type;
}
Index::~Index(){}
