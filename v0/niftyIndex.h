#ifndef NIFTYINDEX_H
#define NIFTYINDEX_H

#include"indexinterface.h"

#include<string>

class NiftyIndex: public Index{

public :
NiftyIndex();
NiftyIndex(std::string,int,int,int,int,int,int,double,double,double,int);

void display()const;
};
#endif
