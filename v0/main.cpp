#include "niftyIndex.h"
#include "indexinterface.h"
#include "Indexcontainer.h"
#include<iostream>


int main(){

    IndexContainer container;
    std::string date;
    std::string open;
    std::string close;
    std::string high;
    std::string low;
    std::string volume;
    std::string turnover;
    std::string PE;
    std::string PB;
    std::string div;
    std::string type;
   
    std::ifstream ip("data.txt");
    if(!ip.is_open()){
        std::cout<<"error in loading data file";
    }
    std::string line;
    while(getline(ip,line)){
       std::stringstream ss(line);

        getline(ss,date,',');
        getline(ss,open,',');
        getline(ss,close,',');
        getline(ss,high,',');
        getline(ss,low,',');
        getline(ss,volume,',');
        getline(ss,turnover,',');
        getline(ss,PE,',');
        getline(ss,PB,',');
        getline(ss,div,',');
        getline(ss,type,',');

        container.addNiftyIndex(date,atoi(open.c_str()),atoi(close.c_str()),atoi(high.c_str()),atoi(low.c_str()),
        atoi(volume.c_str()),atoi(turnover.c_str()),(double) atof(PE.c_str()),(double) atof(PB.c_str()),
        (double) atof(div.c_str()),atoi(type.c_str()));

       

    }
    ip.close();
    std::cout<<std::endl;

    double averagePriceEarningRatio=container.findAverageOfPriceEarningRatio(1);

    if(averagePriceEarningRatio==-1){
        std::cout<<"no data found for averagePriceEarningRatio"<<std::endl;
    }
    else{
    std::cout<<"average PE is:"<<averagePriceEarningRatio<<std::endl;
    }
    
    double maxDividend=container.findMaximumDividend(1);
    if(maxDividend==-1){
        std::cout<<"no maxDividend found"<<std::endl;;
    }
    else{
    std::cout<<"maximum dividend is :"<<maxDividend<<std::endl;
    }

    int res=container.findMaximumOpenValueBetweenTwoDates(1,"01/02/2010","05/02/2010");
    if(res==-1){
        std::cout<<"no MaximumOpenValuedata found Between Two Dates"<<std::endl;
    }
    else{
        std::cout<<"maximum open value between two dates is :"<<res<<std::endl;
    }
    
    double averageTurnoverBetweenDates=container.findAverageTurnOverBetweenTwoDates(1,"01/01/2010","08/03/2010");
    if(averageTurnoverBetweenDates==-1){
        std::cout<<"no average Turnover data found in given dates"<<std::endl;
    }
    else{
        std::cout<<"average average Turnover between two dates is :"<<averageTurnoverBetweenDates<<std::endl;
    }
   
    return 0;
}
