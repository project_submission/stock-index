#include "niftyIndex.h"
#include<string>

NiftyIndex::NiftyIndex(){}

NiftyIndex::NiftyIndex(std::string date, int open ,int close ,int high, int low, int volume,
 int turnover, double pe,double pb, double div, int type):
 Index(date,open,close,high,low,volume,turnover,pe,pb,div,type){}

void NiftyIndex::display()const{
    std::cout<<"date is :"<<Index::getDate()<<std::endl;
    std::cout<<"open value is  :"<<Index::getOpenValue()<<std::endl;
    std::cout<<"close value is  :"<<Index::getCloseValue()<<std::endl;
    std::cout<<"high value is :"<<Index::getHighValue()<<std::endl;
    std::cout<<"low value  is :"<<Index::getLowValue()<<std::endl;
    std::cout<<"volume is  :"<<Index::getVolume()<<std::endl;
    std::cout<<"turnover  is :"<<Index::getTurnover()<<std::endl;
    std::cout<<"price earning ratio is  :"<<Index::getPriceEarningRatio()<<std::endl;
    std::cout<<"price by book ratio  is :"<<Index::getPriceBookRatio()<<std::endl;
    std::cout<<"dividend   is :"<<Index::getDividend()<<std::endl;
    std::cout<<"type of niftyindex  is :"<<Index::getType()<<std::endl;
}
